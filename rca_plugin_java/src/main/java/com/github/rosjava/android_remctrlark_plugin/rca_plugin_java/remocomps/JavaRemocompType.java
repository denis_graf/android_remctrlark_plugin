/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.android_remctrlark_plugin.rca_plugin_java.remocomps;

import android.view.View;
import android.widget.Toast;

import com.github.rosjava.android_remctrlark.rcajava.binders.annotations.RcaType;
import com.github.rosjava.android_remctrlark.rcajava.configuration.IJavaRemoCompConfiguration;
import com.github.rosjava.android_remctrlark.rcajava.context.IJavaRemoCompContext;
import com.github.rosjava.android_remctrlark.rcajava.misc.ICheckedRunnable;
import com.github.rosjava.android_remctrlark.rcajava_impl.AJavaRemoCompImplBase;
import com.github.rosjava.android_remctrlark.rcajava_impl.error_handling.RosNodeError;
import com.github.rosjava.android_remctrlark_plugin.rca_plugin_java.R;

import org.ros.namespace.GraphName;
import org.ros.node.AbstractNodeMain;
import org.ros.node.ConnectedNode;
import org.ros.node.Node;
import org.ros.node.NodeMain;
import org.ros.node.topic.Publisher;

@RcaType("This an example implementation of a RemoComp.\n" +

		"\nParameters:\n" +
		"\t" + JavaRemocompType.PARAM_NAME_MESSAGE + " (str): Message to be published. " +
		"Default: " + JavaRemocompType.PARAM_DVALUE_MESSAGE + "\n" +

		"\nPublished topics:\n" +
		"\t" + JavaRemocompType.TOPIC_NAME + " (std_msgs/String): Publishes the specified message.")
public class JavaRemocompType extends AJavaRemoCompImplBase {

	public static final String PARAM_NAME_MESSAGE = "message";
	public static final String PARAM_DVALUE_MESSAGE = "<no message defined>";
	public static final String TOPIC_NAME = "chatter";

	private String mMessage = null;
	private NodeMain mRosNode = null;
	private Publisher<std_msgs.String> mPublisher = null;

	@Override
	public void init(IJavaRemoCompContext context) throws Throwable {
		super.init(context);
		mMessage = getConfig().getParamString(PARAM_NAME_MESSAGE, PARAM_DVALUE_MESSAGE);
	}

	@Override
	public void createUI() {
		getLayout().getInflater().inflate(R.layout.java_remocomp_type, getParentView());
	}

	@Override
	public void start(IJavaRemoCompConfiguration configuration) {
		mRosNode = new AbstractNodeMain() {
			@Override
			public GraphName getDefaultNodeName() {
				return GraphName.of(getRcaContext().getType());
			}

			@Override
			public void onStart(ConnectedNode connectedNode) {
				mPublisher = connectedNode.newPublisher(TOPIC_NAME, std_msgs.String._TYPE);
				getParentView().findViewById(R.id.button_remocomp).setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						getRcaContext().postToImplThread(new ICheckedRunnable() {
							@Override
							public void run() {
								if (debug) {
									getLog().d("sending message '" + mMessage + "'");
								}
								if (mPublisher != null) {
									final std_msgs.String message = mPublisher.newMessage();
									message.setData(mMessage);
									mPublisher.publish(message);
									getRcaContext().postToUiThread(new ICheckedRunnable() {
										@Override
										public void run() {
											Toast.makeText(getApkContext(), mMessage, Toast.LENGTH_SHORT).show();
										}
									});
								}
							}
						});
					}
				});
			}

			@Override
			public void onShutdown(Node node) {
				getRcaContext().postToImplThread(new ICheckedRunnable() {
					@Override
					public void run() {
						shutdown();
					}
				});
			}

			@Override
			public void onError(final Node node, final Throwable throwable) {
				getRcaContext().handleException(new RosNodeError(node, throwable));
			}
		};

		getNodeExecutor().execute(mRosNode, configuration.createNodeConfiguration(getRcaContext().getType()));
	}

	@Override
	public void shutdown() {
		if (mRosNode != null) {
			getParentView().findViewById(R.id.button_remocomp).setOnClickListener(null);
			getNodeExecutor().shutdownNodeMain(mRosNode);
			mPublisher = null;
			mRosNode = null;
		}
	}
}
