# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

import rospy
import std_msgs.msg

import rcapy_impl


class Impl(rcapy_impl.APyRemoCompImplBase):
    """
    This an example implementation of a RemoComp.

    Parameters:
        message (str): Message to be published. Default: <no message defined>

    Published topics:
        chatter (std_msgs/String): Publishes the specified message.
    """

    PARAM_NAME_MESSAGE = "message"
    PARAM_DVALUE_MESSAGE = "<no message defined>"
    TOPIC_NAME = "chatter"

    def __init__(self, context):
        """
        @type context: APyRemoCompContext
        """
        super(Impl, self).__init__(context)
        self._droid = self.context.create_android_proxy()
        self._publisher = None
        """@type: rospy.Publisher"""

        self._message = self.config.get_param(self.PARAM_NAME_MESSAGE, self.PARAM_DVALUE_MESSAGE)

    def start(self, configuration):
        """
        @type configuration: APyRemoCompConfiguration
        """
        rospy.init_node(*configuration.create_node_configuration(self.context.the_type))
        self._publisher = rospy.Publisher(self.TOPIC_NAME, std_msgs.msg.String)
        self.parent_view.set_on_click_listener("button_remocomp", self.on_click_button_remocomp)

    def on_click_button_remocomp(self, name, data):
        """
        @type name: str
        @type data: dict[str, object]
        """
        assert name == "click"
        assert data["id"] == "button_remocomp"
        if self.debug:
            self.log.d("sending message '%s'" % self._message)
        self._publisher.publish(self._message)
        self._droid.sl4a.makeToast(self._message)
